package com.example.paragontest.osdictionary.model;

import android.content.res.XmlResourceParser;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public interface MasterInteractor {
    interface MasterInteractorResult {
        void onOsListLoaded(List<OsItem> osItemList);
        void onOsItemDeleted();
        void onOsItemUpdated();
    }

    interface XmlParseResult {
        void onXmlParseComplete(List<OsItem> osList);
    }

    interface OsLabResult {
        void onGetAllResult(List<OsItem> result);
        void onAddOsListComplete();
        void onOsItemDeleted();
        void onDbCreated();
        void onOsItemUpdated();
    }

    void getOsList(int method);
    void deleteOsItem(OsItem osItem);
    void updateOsItem(OsItem osItem);
}
