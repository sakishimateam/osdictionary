package com.example.paragontest.osdictionary.model;

import android.content.res.XmlResourceParser;
import android.util.Log;

import com.example.paragontest.osdictionary.model.processing.ParsingXml;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class MasterInteractorImpl implements MasterInteractor, MasterInteractor.XmlParseResult, MasterInteractor.OsLabResult {
    private static final String TAG = "MasterInteractorImpl";
    private OsLab osLab;
    private MasterInteractorResult interactorResult;
    private volatile XmlResourceParser xmlParser;

    public MasterInteractorImpl(XmlResourceParser xmlParser, MasterInteractorResult interactorResult) {
        this.xmlParser = xmlParser;
        this.interactorResult = interactorResult;
        this.osLab = OsLab.getLab();
        osLab.setResultObject(this);
    }

    @Override
    public void onDbCreated() {
        Log.i(TAG, "It is new database");
        new Thread(() -> {
            while (xmlParser == null);
            ParsingXml.beginParse(xmlParser, this);
        }).start();
    }

    @Override
    public void onXmlParseComplete(List<OsItem> osList) {
        Log.i(TAG, "XML parse return size: " + osList.size());
        osLab.addOsList(osList);
    }

    @Override
    public void getOsList(int method) {
        Log.i(TAG, "It is old database");
        osLab.getOsList(method);
    }

    @Override
    public void deleteOsItem(OsItem osItem) {
        osLab.deleteOsItem(osItem);
    }

    @Override
    public void onOsItemUpdated() {
        interactorResult.onOsItemUpdated();
    }

    @Override
    public void updateOsItem(OsItem osItem) {
        osLab.updateOsItem(osItem);
    }

    @Override
    public void onOsItemDeleted() {
        interactorResult.onOsItemDeleted();
    }

    @Override
    public void onGetAllResult(List<OsItem> result) {
        Log.i(TAG, "Db return data: " + result.size());
        interactorResult.onOsListLoaded(result);
    }

    @Override
    public void onAddOsListComplete() {
        osLab.getOsList(OsLab.GET_ALL_OS_LIST);
    }
}
