package com.example.paragontest.osdictionary.model;

import java.io.Serializable;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class OsItem implements Serializable {
    private long id;
    private String version;
    private String name;
    private String released;
    private int api;
    private float distribution;
    private String description;
    private boolean favorite = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public int getApi() {
        return api;
    }

    public void setApi(int api) {
        this.api = api;
    }

    public float getDistribution() {
        return distribution;
    }

    public void setDistribution(float distribution) {
        this.distribution = distribution;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public boolean equals(Object obj) {
        if (getClass() != obj.getClass()) {
            return false;
        }
        OsItem item = (OsItem) obj;
        return this.id == item.getId()
                && this.favorite == item.favorite
                && this.version.equals(item.version)
                && this.name.equals(item.name)
                && this.released.equals(item.released)
                && this.api == item.api
                && this.distribution == item.distribution
                && this.description.equals(item.description);
    }
}
