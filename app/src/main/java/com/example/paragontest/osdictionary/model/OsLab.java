package com.example.paragontest.osdictionary.model;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.example.paragontest.osdictionary.model.database.DatabaseStatus;
import com.example.paragontest.osdictionary.model.database.DbManager;
import com.example.paragontest.osdictionary.model.database.OsTable;
import com.example.paragontest.osdictionary.model.database.OsTableDao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class OsLab implements DatabaseStatus {
    private static OsLab osLab;
    public static final int GET_ALL_OS_LIST = 0;
    public static final int GET_FAVORITES_OS_LIST = 1;
    public static final int GET_DISTRIBUTION_OS_LIST = 2;
    private MasterInteractor.OsLabResult result;
    private DbManager db;

    public static void newLab(Context context) {
        if (osLab == null) {
            osLab = new OsLab(context);
        }
    }

    public static OsLab getLab() {
        return osLab;
    }

    public void setResultObject(MasterInteractor.OsLabResult result) {
        this.result = result;
    }

    private OsLab(Context context) {
        db = DbManager.currentManager(context);
    }

    public void updateOsItem(OsItem osItem) {
        new Thread(() -> {
            OsTableDao osTableDao = db.getDatabase().osTableDao();
            osTableDao.update(convertItemToTable(osItem));
            new Handler(Looper.getMainLooper()).post(result::onOsItemUpdated);
        }).start();
    }

    public void deleteOsItem(OsItem osItem) {
        new Thread(() -> {
            OsTableDao osTableDao = db.getDatabase().osTableDao();
            osTableDao.delete(convertItemToTable(osItem));
            new Handler(Looper.getMainLooper()).post(result::onOsItemDeleted);
        }).start();
    }

    public void addOsList(List<OsItem> osItemList) {
        new Thread(() -> {
            OsTableDao osTableDao = db.getDatabase().osTableDao();
            for (OsItem osItem : osItemList) {
                osTableDao.insert(convertItemToTable(osItem));
            }
            new Handler(Looper.getMainLooper()).post(result::onAddOsListComplete);
        }).start();
    }

    @Override
    public void onDbCreate() {
        result.onDbCreated();
    }

    public void getOsList(int method) {
        new Thread(() -> {
            OsTableDao osTableDao = db.getDatabase().osTableDao();
            List<OsTable> osTableList;
            switch (method) {
                case GET_ALL_OS_LIST:
                    osTableList = osTableDao.getAll();
                    break;
                case GET_FAVORITES_OS_LIST:
                    osTableList = osTableDao.getFavorites();
                    break;
                case GET_DISTRIBUTION_OS_LIST:
                    osTableList = osTableDao.getDistribution();
                    break;
                default:
                    osTableList = osTableDao.getAll();
            }

            List<OsItem> osItemList = new ArrayList<>();
            for (OsTable osTable : osTableList) {
                osItemList.add(convertTabletoItem(osTable));
            }
            new Handler(Looper.getMainLooper()).post(() -> {
                result.onGetAllResult(osItemList);
            });

        }).start();
    }

    private OsTable convertItemToTable(OsItem osItem) {
        OsTable osTable = new OsTable();
        if (osItem != null) {
            osTable.id = osItem.getId();
            osTable.version = osItem.getVersion();
            osTable.name = osItem.getName();
            osTable.released = osItem.getReleased();
            osTable.api = osItem.getApi();
            osTable.distribution = osItem.getDistribution();
            osTable.description = osItem.getDescription();
            osTable.favorite = osItem.isFavorite();
        }
        return osTable;
    }

    private OsItem convertTabletoItem(OsTable osTable) {
        OsItem osItem = new OsItem();
        if (osTable != null) {
            osItem.setId(osTable.id);
            osItem.setVersion(osTable.version);
            osItem.setName(osTable.name);
            osItem.setReleased(osTable.released);
            osItem.setApi(osTable.api);
            osItem.setDistribution(osTable.distribution);
            osItem.setDescription(osTable.description);
            osItem.setFavorite(osTable.favorite);
        }
        return osItem;
    }
}
