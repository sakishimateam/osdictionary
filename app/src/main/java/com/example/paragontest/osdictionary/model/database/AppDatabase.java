package com.example.paragontest.osdictionary.model.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by
 *
 * @author Sakishima (24.05.2018)
 */
@Database(entities = {OsTable.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract OsTableDao osTableDao();
}
