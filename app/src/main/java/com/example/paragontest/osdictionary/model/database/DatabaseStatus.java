package com.example.paragontest.osdictionary.model.database;

/**
 * Created by
 *
 * @author Sakishima (25.05.2018)
 */
public interface DatabaseStatus {
    void onDbCreate();
}
