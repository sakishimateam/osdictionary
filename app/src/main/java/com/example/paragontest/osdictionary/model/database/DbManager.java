package com.example.paragontest.osdictionary.model.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.paragontest.osdictionary.model.OsLab;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class DbManager {
    private static final String TAG = "DbManager";
    private static DbManager dbManager;
    private AppDatabase db;

    public static DbManager currentManager(Context context) {
        if (dbManager == null) {
            dbManager = new DbManager(context);
        }
        return dbManager;
    }

    private DbManager(Context context) {
        if (db == null) {
            db = Room
                    .databaseBuilder(context, AppDatabase.class, "database")
                    .addCallback(dbCallback)
                    .build();
        }
    }

    private RoomDatabase.Callback dbCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            //super.onCreate(db);
            Log.i(TAG, "on create database callback");
            DatabaseStatus databaseStatus = OsLab.getLab();
            databaseStatus.onDbCreate();
        }
    };

    public AppDatabase getDatabase() {
        return db;
    }
}
