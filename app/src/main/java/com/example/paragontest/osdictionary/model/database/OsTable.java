package com.example.paragontest.osdictionary.model.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
@Entity
public class OsTable {
    @PrimaryKey(autoGenerate = true)
    public long id;

    public String version;

    public String name;

    public String released;

    public int api;

    public float distribution;

    public String description;

    public boolean favorite;
}
