package com.example.paragontest.osdictionary.model.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
@Dao
public interface OsTableDao {
    @Query("SELECT * FROM OsTable")
    List<OsTable> getAll();

    @Query("SELECT * FROM OsTable WHERE favorite = 1")
    List<OsTable> getFavorites();

    @Query("SELECT * FROM OsTable WHERE distribution < 3")
    List<OsTable> getDistribution();

    @Query("SELECT * FROM OsTable WHERE id = :id")
    OsTable getById(long id);

    @Insert
    void insert(OsTable osTable);

    @Update
    void update(OsTable osTable);

    @Delete
    void delete(OsTable osTable);
}
