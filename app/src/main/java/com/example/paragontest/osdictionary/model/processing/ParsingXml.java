package com.example.paragontest.osdictionary.model.processing;

import android.content.res.XmlResourceParser;
import android.util.Log;

import com.example.paragontest.osdictionary.model.MasterInteractor;
import com.example.paragontest.osdictionary.model.OsItem;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class ParsingXml {
    private static XmlPullParser parser;
    private static List<OsItem> osItemList;

    public static void beginParse(XmlResourceParser xmlParser, MasterInteractor.XmlParseResult result) {
        osItemList = new ArrayList<>();
        parser = xmlParser;
        new Thread(() -> {
            try {
                OsItem osItem = null;
                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    String tagName;
                    switch (eventType) {
                        case XmlPullParser.START_TAG:
                            tagName = parser.getName();
                            if (tagName.equals("os_node")) {
                                osItem = new OsItem();
                            } else switch (tagName) {
                                case "version":
                                    osItem.setVersion(parser.nextText());
                                    break;
                                case "name":
                                    osItem.setName(parser.nextText());
                                    break;
                                case "released":
                                    osItem.setReleased(parser.nextText());
                                    break;
                                case "api":
                                    osItem.setApi(Integer.parseInt(parser.nextText()));
                                    break;
                                case "distribution":
                                    osItem.setDistribution(Float.parseFloat(parser.nextText()));
                                    break;
                                case "description":
                                    osItem.setDescription(parser.nextText());
                                    break;
                            }
                            break;
                        case XmlPullParser.END_TAG:
                            tagName = parser.getName();
                            if (tagName.equals("os_node")) {
                                osItemList.add(osItem);
                            }
                            break;
                    }
                    eventType = parser.next();
                }
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            result.onXmlParseComplete(osItemList);
        }).start();
    }
}
