package com.example.paragontest.osdictionary.presenter.master;

import android.content.res.XmlResourceParser;

import com.example.paragontest.osdictionary.model.OsItem;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public interface MasterPresenter {
    void getOsItemList(int method);
    void deleteOsItem(OsItem osItem);
    void updateOsItem(OsItem osItem);
}
