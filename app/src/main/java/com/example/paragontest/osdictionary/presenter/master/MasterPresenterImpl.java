package com.example.paragontest.osdictionary.presenter.master;

import android.content.Context;
import android.content.res.XmlResourceParser;

import com.example.paragontest.osdictionary.model.MasterInteractor;
import com.example.paragontest.osdictionary.model.MasterInteractorImpl;
import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.model.OsLab;
import com.example.paragontest.osdictionary.view.master.ResultToFragment;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class MasterPresenterImpl implements MasterPresenter, MasterInteractor.MasterInteractorResult {
    private MasterInteractor masterInteractorCommand;
    private ResultToFragment.ResultFromPresenter resultToFragment;

    public MasterPresenterImpl(Context context, XmlResourceParser xmlParser, ResultToFragment.ResultFromPresenter resultToFragment) {
        this.resultToFragment = resultToFragment;
        OsLab.newLab(context);
        masterInteractorCommand = new MasterInteractorImpl(xmlParser, this);
    }

    @Override
    public void deleteOsItem(OsItem osItem) {
        masterInteractorCommand.deleteOsItem(osItem);
    }

    @Override
    public void getOsItemList(int method) {
        masterInteractorCommand.getOsList(method);
    }

    @Override
    public void onOsListLoaded(List<OsItem> osItemList) {
        resultToFragment.osListFromDatabase(osItemList);
    }

    @Override
    public void onOsItemDeleted() {
        resultToFragment.onOsItemDeleted();
    }

    @Override
    public void updateOsItem(OsItem osItem) {
        masterInteractorCommand.updateOsItem(osItem);
    }

    @Override
    public void onOsItemUpdated() {
        resultToFragment.onOsItemUpdated();
    }
}
