package com.example.paragontest.osdictionary.view.detail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;

import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.view.SingleFragmentActivity;

/**
 * Created by
 *
 * @author Sakishima (25.05.2018)
 */
public class DetailActivity extends SingleFragmentActivity {
    public static final String EXTRA_OS_DETAIL = "com.example.paragontest.osdictionary.osdetail";
    public static final String EXTRA_OS_DETAIL_RESULT = "com.example.paragontest.osdictionary.osdetailresult";
    private DetailFragment detailFragment;

    public static Intent newIntent(Context context, OsItem osItem) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_OS_DETAIL, osItem);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        detailFragment = new DetailFragment();
        return detailFragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_OS_DETAIL_RESULT, detailFragment.getOsItem());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
