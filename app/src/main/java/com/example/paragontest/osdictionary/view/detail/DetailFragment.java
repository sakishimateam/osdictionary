package com.example.paragontest.osdictionary.view.detail;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.paragontest.osdictionary.R;
import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.view.master.MasterFragment;
import com.example.paragontest.osdictionary.view.master.ResultToFragment;

/**
 * Created by
 *
 * @author Sakishima (25.05.2018)
 */
public class DetailFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, ResultToDetailFragment {
    private static final String ARG_OS_ITEM = "os_item";
    private OsItem osItem;
    private TextView osVersionTextView;
    private TextView osNameTextView;
    private TextView osReleasedTextView;
    private TextView osApiTextView;
    private TextView osDistributionTextView;
    private TextView osDescriptionTextView;
    private ToggleButton osFavoriteButton;
    private static ResultToFragment.ResultFromDetailFragment detailResult;

    public static DetailFragment newInstance(OsItem osItem, ResultToFragment.ResultFromDetailFragment detailResult) {
        DetailFragment.detailResult = detailResult;

        Bundle args = new Bundle();
        args.putSerializable(ARG_OS_ITEM, osItem);

        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            if (getArguments() != null) {
                osItem = (OsItem) getArguments().getSerializable(ARG_OS_ITEM);
            } else {
                Intent intent = getActivity().getIntent();
                if (intent != null) {
                    osItem = (OsItem) intent.getSerializableExtra(DetailActivity.EXTRA_OS_DETAIL);
                }
            }
        }
        if (MasterFragment.isTablet) {
            detailResult.detailFragmentResultObject(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        osVersionTextView = view.findViewById(R.id.detail_os_version_text_view);
        osNameTextView = view.findViewById(R.id.detail_os_name_text_view);
        osReleasedTextView = view.findViewById(R.id.detail_os_released_text_view);
        osApiTextView = view.findViewById(R.id.detail_os_api_text_view);
        osDistributionTextView = view.findViewById(R.id.detail_os_distribution_text_view);
        osDescriptionTextView = view.findViewById(R.id.detail_os_description_text_view);
        osFavoriteButton = view.findViewById(R.id.detais_os_favorite_button);

        osVersionTextView.setText(osItem.getVersion());
        osNameTextView.setText(osItem.getName());
        osReleasedTextView.setText(osItem.getReleased());
        String apiString = "API " + String.valueOf(osItem.getApi());
        osApiTextView.setText(apiString);
        String distributionString = String.valueOf(osItem.getDistribution()) + "%";
        osDistributionTextView.setText(distributionString);
        osDescriptionTextView.setText(osItem.getDescription());

        osFavoriteButton.setOnCheckedChangeListener(null);
        osFavoriteButton.setChecked(osItem.isFavorite());
        osFavoriteButton.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        osItem.setFavorite(isChecked);
        if (detailResult != null) {
            detailResult.onDetailFavoriteClick(osItem);
        }
    }

    public OsItem getOsItem() {
        return osItem;
    }

    @Override
    public void onFavoriteChange(boolean favoriteState) {
        osFavoriteButton.setChecked(favoriteState);
    }
}
