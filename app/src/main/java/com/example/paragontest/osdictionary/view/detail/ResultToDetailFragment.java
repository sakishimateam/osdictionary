package com.example.paragontest.osdictionary.view.detail;

/**
 * Created by
 *
 * @author Sakishima (26.05.2018)
 */
public interface ResultToDetailFragment {
    void onFavoriteChange(boolean favoriteState);
}
