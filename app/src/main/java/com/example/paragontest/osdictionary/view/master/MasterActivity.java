package com.example.paragontest.osdictionary.view.master;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.example.paragontest.osdictionary.R;
import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.view.SingleFragmentActivity;
import com.example.paragontest.osdictionary.view.detail.DetailActivity;
import com.example.paragontest.osdictionary.view.detail.DetailFragment;

public class MasterActivity extends SingleFragmentActivity implements MasterFragment.FragmentCallbacks {
    private static final int REQUEST_DETAIL = 0;
    private MasterFragment masterFragment;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        masterFragment = MasterFragment.newInstance();
        return masterFragment;
    }

    @Override
    public void onOsSelected(OsItem osItem) {
        if (findViewById(R.id.detail_fragment_container) == null) {
            //Телефонный режим
            Intent intent = DetailActivity.newIntent(this, osItem);
            startActivityForResult(intent, REQUEST_DETAIL);
        } else {
            //Режим планшета (два фрагмента на экране)
            Fragment newDetail = DetailFragment.newInstance(osItem, masterFragment);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, newDetail)
                    .commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_DETAIL:
                OsItem osItem = (OsItem) data.getSerializableExtra(DetailActivity.EXTRA_OS_DETAIL_RESULT);
                masterFragment.onDetailFavoriteClick(osItem);
        }
    }
}
