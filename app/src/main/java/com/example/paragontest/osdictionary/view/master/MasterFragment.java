package com.example.paragontest.osdictionary.view.master;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.paragontest.osdictionary.R;
import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.model.OsLab;
import com.example.paragontest.osdictionary.presenter.master.MasterPresenter;
import com.example.paragontest.osdictionary.presenter.master.MasterPresenterImpl;
import com.example.paragontest.osdictionary.view.detail.DetailFragment;
import com.example.paragontest.osdictionary.view.detail.ResultToDetailFragment;

import java.util.List;
import java.util.Objects;

/**
 * Created by
 *
 * @author Sakishima (23.05.2018)
 */
public class MasterFragment extends Fragment implements
        ResultToFragment.ResultFromPresenter,
        ResultToFragment.ResultFromHolder,
        ResultToFragment.ResultFromDetailFragment {

    private static final String TAG = "MasterFragment";
    MasterPresenter presenterSendCommand;
    public static boolean isTablet = false;
    private RecyclerView osRecyclerView;
    private OsAdapter osAdapter;
    private FragmentCallbacks callbackToActivity;
    private ResultToDetailFragment callbackToDetailFragment;
    private int menuItemChecked = 0;

    public interface FragmentCallbacks {
        void onOsSelected(OsItem osItem);
    }

    public static MasterFragment newInstance() {
        return new MasterFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        XmlResourceParser xmlParser = getResources().getXml(R.xml.os_catalog);
        presenterSendCommand = new MasterPresenterImpl(getContext(), xmlParser, this);
        MasterFragment.isTablet = getResources().getBoolean(R.bool.isTablet);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container, false);

        osRecyclerView = view.findViewById(R.id.oslist_recycler_veiw);
        osRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Objects.requireNonNull(getActivity()).setTitle(R.string.menu_all_text);

        presenterSendCommand.getOsItemList(OsLab.GET_ALL_OS_LIST);

        return view;
    }

    @Override
    public void osListFromDatabase(List<OsItem> osItemList) {
        if (osItemList != null) {
            if (osAdapter == null) {
                osAdapter = new OsAdapter(osItemList, this);
                osRecyclerView.setAdapter(osAdapter);
            } else {
                osAdapter.setOsItemList(osItemList);
            }
            Log.i(TAG, "Data from db: " + osItemList.size());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_master_screen, menu);
    }

    @Override
    public void onOsItemDeleted() {
        presenterSendCommand.getOsItemList(menuItemChecked);
    }

    @Override
    public void onOsItemUpdated() {
        osAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(OsItem osItem) {
        callbackToActivity.onOsSelected(osItem);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onFavoriteClick(OsItem osItem) {
        Log.i(TAG, "Favorite click result");
        if (MasterFragment.isTablet) {
            if (callbackToDetailFragment != null) {
                callbackToDetailFragment.onFavoriteChange(osItem.isFavorite());
            }
        }
        presenterSendCommand.updateOsItem(osItem);
    }

    @Override
    public void onItemDeleteClick(OsItem osItem) {
        presenterSendCommand.deleteOsItem(osItem);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbackToActivity = (FragmentCallbacks) context;
    }

    @Override
    public void onDetailFavoriteClick(OsItem osItem) {
        Log.i(TAG, "Detail favorite click result");
        if (!MasterFragment.isTablet) {
            if (!osAdapter.updateItem(osItem)) {
                return;
            }
        }
        presenterSendCommand.updateOsItem(osItem);
    }

    @Override
    public void clearDetailFragment() {
        if (getActivity() != null) {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.detail_fragment_container);
            if (fragment != null) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
    }

    @Override
    public void detailFragmentResultObject(ResultToDetailFragment detailResult) {
        callbackToDetailFragment = detailResult;
    }

    private void clearSelectedElementsFromAdapter() {
        if (MasterFragment.isTablet) {
            clearDetailFragment();
            osAdapter.setSelectedPosition(-1);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (getActivity() == null) {
            return super.onOptionsItemSelected(item);
        }
        switch (item.getItemId()) {
            case R.id.menu_item_all:
                getActivity().setTitle(R.string.menu_all_text);
                if (!item.isChecked()) {
                    item.setChecked(true);
                    menuItemChecked = OsLab.GET_ALL_OS_LIST;
                    presenterSendCommand.getOsItemList(menuItemChecked);
                    clearSelectedElementsFromAdapter();
                }
                return true;
            case R.id.menu_item_favorites:
                getActivity().setTitle(R.string.menu_favorites_text);
                if (!item.isChecked()) {
                    item.setChecked(true);
                    menuItemChecked = OsLab.GET_FAVORITES_OS_LIST;
                    presenterSendCommand.getOsItemList(menuItemChecked);
                    clearSelectedElementsFromAdapter();
                }
                return true;
            case R.id.menu_item_distribution:
                getActivity().setTitle(R.string.menu_distribution_text);
                if (!item.isChecked()) {
                    item.setChecked(true);
                    menuItemChecked = OsLab.GET_DISTRIBUTION_OS_LIST;
                    presenterSendCommand.getOsItemList(menuItemChecked);
                    clearSelectedElementsFromAdapter();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
