package com.example.paragontest.osdictionary.view.master;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.paragontest.osdictionary.R;
import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.presenter.master.MasterPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (24.05.2018)
 */
public class OsAdapter extends RecyclerView.Adapter<OsHolder> {
    public ResultToFragment.ResultFromHolder resultToFragment;
    private List<OsItem> osItemList;
    private int selectedPosition;

    OsAdapter(List<OsItem> osItemList, ResultToFragment.ResultFromHolder result) {
        resultToFragment = result;
        selectedPosition = -1;
        if (osItemList == null) {
            osItemList = new ArrayList<>();
            osItemList.add(new OsItem());
        } else {
            this.osItemList = osItemList;
        }
    }

    public void setOsItemList(List<OsItem> osItemList) {
        this.osItemList = osItemList;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.list_item_os, parent, false);
        return new OsHolder(v, this);
    }

    @Override
    public void onBindViewHolder(@NonNull OsHolder holder, int position) {
        OsItem osItem = osItemList.get(position);
        holder.itemView.setBackgroundColor(selectedPosition == position ? Color.LTGRAY : Color.TRANSPARENT);
        holder.bindOsItem(osItem);
    }

    @Override
    public int getItemCount() {
        return osItemList.size();
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public boolean updateItem(OsItem osItem) {
        for (int i = 0; i < osItemList.size(); i++) {
            if (osItem.getId() == osItemList.get(i).getId()) {
                if (!osItemList.get(i).equals(osItem)) {
                    osItemList.get(i).setFavorite(osItem.isFavorite());
                    notifyItemChanged(i);
                    return true;
                }
                break;
            }
        }
        return false;
    }
}
