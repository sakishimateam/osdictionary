package com.example.paragontest.osdictionary.view.master;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.paragontest.osdictionary.R;
import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.presenter.master.MasterPresenter;

/**
 * Created by
 *
 * @author Sakishima (24.05.2018)
 */
public class OsHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener, CompoundButton.OnCheckedChangeListener {
    private OsAdapter adapter;
    private TextView osVersionTextView;
    private TextView osNameTextView;
    private ToggleButton osFavoriteButton;
    private OsItem osItem;

    OsHolder(View itemView, OsAdapter adapter) {
        super(itemView);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
        osVersionTextView = itemView.findViewById(R.id.os_version_text_view);
        osNameTextView = itemView.findViewById(R.id.os_name_text_view);
        osFavoriteButton = itemView.findViewById(R.id.os_favorite_button);
        osFavoriteButton.setOnCheckedChangeListener(this);
    }

    public void bindOsItem(OsItem osItem) {
        this.osItem = osItem;
        osVersionTextView.setText(osItem.getVersion());
        osNameTextView.setText(osItem.getName());
        osFavoriteButton.setOnCheckedChangeListener(null);
        osFavoriteButton.setChecked(osItem.isFavorite());
        osFavoriteButton.setOnCheckedChangeListener(this);
    }

    private void showDeleteDialog() {
        Context context = itemView.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.dialog_title)
                .setMessage(R.string.dialog_message_text);
        builder.setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
            if (MasterFragment.isTablet) {
                adapter.resultToFragment.clearDetailFragment();
                adapter.setSelectedPosition(-1);
            }
            adapter.resultToFragment.onItemDeleteClick(osItem);
        });
        builder.setNegativeButton(R.string.dialog_no, (dialog, which) -> {});

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        if (MasterFragment.isTablet) {
            if (adapter.getSelectedPosition() != -1) {
                adapter.notifyItemChanged(adapter.getSelectedPosition());
            }
            adapter.setSelectedPosition(getAdapterPosition());
            adapter.notifyItemChanged(adapter.getSelectedPosition());
        }
        adapter.resultToFragment.onItemClick(osItem);
    }

    @Override
    public boolean onLongClick(View v) {
        showDeleteDialog();
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        osItem.setFavorite(isChecked);
        adapter.resultToFragment.onFavoriteClick(osItem);
    }
}
