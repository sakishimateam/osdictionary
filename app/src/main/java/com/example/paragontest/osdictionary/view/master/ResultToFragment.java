package com.example.paragontest.osdictionary.view.master;

import com.example.paragontest.osdictionary.model.OsItem;
import com.example.paragontest.osdictionary.view.detail.ResultToDetailFragment;

import java.util.List;

/**
 * Created by
 *
 * @author Sakishima (24.05.2018)
 */
public interface ResultToFragment {
    interface ResultFromPresenter {
        void osListFromDatabase(List<OsItem> osItemList);
        void onOsItemDeleted();
        void onOsItemUpdated();
    }

    interface ResultFromHolder {
        void onItemClick(OsItem osItem);
        void onFavoriteClick(OsItem osItem);
        void onItemDeleteClick(OsItem osItem);
        void clearDetailFragment();
    }

    interface ResultFromDetailFragment {
        void onDetailFavoriteClick(OsItem osItem);
        void detailFragmentResultObject(ResultToDetailFragment detailResult);
    }
}
